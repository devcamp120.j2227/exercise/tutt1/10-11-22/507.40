const express = require("express");
const app = express();
const port = 8000;

const path = require("path");

app.use(express.static(__dirname + "/view/Pizza 365"));

app.get("/pizza365", (req, res) => {
    console.log(__dirname);
    // Hiển thị project ra localhost8000
    res.sendFile(path.join(__dirname + "/view/Pizza 365/index.html"));
});

app.listen(port, () => {
    console.log("App listening on port: ", port);
});