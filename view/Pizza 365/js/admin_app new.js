$(document).ready(function(){
  //Region 1: vùng khai báo biến toàn cục
  var gId;
  var gOrderId;
  var gPizzaSize = ["S","M","L"];
  var gDataOrder1 = [];
  var gDataOrder = [];// khai báo biến mảng để chứa thông tin đơn hàng
  const gORDER = ["orderId", "kichCo","loaiPizza","idLoaiNuocUong", "thanhTien", "hoTen","soDienThoai","trangThai", "action"];
  //khai báo cột của datatable
  const gORDER_ID = 0;
  const gKICH_CO = 1;
  const gLOAI_PIZZA = 2;
  const gID_NUOC_UONG = 3;
  const gTHANH_TIEN= 4;
  const gHO_TEN = 5;
  const gSO_DIEN_THOAI= 6;
  const gTRANG_THAI = 7;
  const gACITON = 8;
  //định nghĩa datatable(chưa có dữ liệu)
  $("#tbl-order").DataTable({
    stateSave: true,
    columns:[
      {data: gORDER[gORDER_ID]},
      {data: gORDER[gKICH_CO]},
      {data: gORDER[gLOAI_PIZZA]},
      {data: gORDER[gID_NUOC_UONG]},
      {data: gORDER[gTHANH_TIEN]},
      {data: gORDER[gHO_TEN]},
      {data: gORDER[gSO_DIEN_THOAI]},
      {data: gORDER[gTRANG_THAI]},
      {data: gORDER[gACITON]},
    ],
    columnDefs:[
      {
        targets: gACITON,
        defaultContent: '<button class="btn btn-success btn-detail" style="width: 100px;"><i class="fa-solid fa-circle-info"></i> Chi tiết</button>',
      }, 
     
    ],
    
  })
  //Region 2: vùng gán thực thi hàm xử lý sự liện
  onPageLoading();
  $("#tbl-order").on("click",".btn-detail",function(){
    onBtnDetailClick(this);// this là button được ấn
  });
  $("#btn-filter-order").on("click", onBtnFilterClick);
  loadDrinkList()
  getPizzaSize(gPizzaSize);
  $("#btn-confirm").on("click", onBtnConfirmClick);
  $("#btn-cancel").on("click", onBtnCancelClick);
  
  //Region 3: vùng khai báo các hàm xử lý sự kiện
  function onPageLoading(){
    //lấy data từ api
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
      type: "GET",
      dataType: "json",
      success:function(res){
        gDataOrder = res;
        console.log(gDataOrder);
        loadDataToTable(gDataOrder);
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });
  }

  function onBtnFilterClick(){
    "use strict";
    var vStatus = $("#select-status").val();
    var vPizzaType = $("#select-type-pizza").val();
    
    var vDataFilter = gDataOrder.filter(function (paramOrder, index) {
          return(vStatus === "all" || (paramOrder.trangThai != null && vStatus.toUpperCase() === paramOrder.trangThai.toUpperCase()))
          && (vPizzaType === "all" || (paramOrder.loaiPizza != null && vPizzaType.toUpperCase() === paramOrder.loaiPizza.toUpperCase()))
      });

    loadDataToTable(vDataFilter);
  }
  
  //hàm xử lí khi nút confirm được click
  function onBtnConfirmClick(){
    console.log("ID: " + gId);
    var vObjectRequest = {
          trangThai: "confirmed",
          id: 0,
          orderId: "",
          kichCo: "",
          duongKinh: "",
          loaiPizza: "",
          idVourcher: "",
          giamGia: "",
          idLoaiNuocUong: "",
          soLuongNuoc: "",
          hoTen: "",
          email: "",
          soDienThoai: "",
          loiNhan: "",
          ngayTao: "",
          ngayCapNhat: "",
    }
    //b1: thu thập dữ liệu
    getData(vObjectRequest);
    console.log(vObjectRequest);
    //b2 check dữ liệu
    //b3 call api
    putApi(vObjectRequest);
    //b4: load lại dữ liệu để kiểm tra cập nhật
    reloadDataToTable(vObjectRequest);
  }

  //hàm xử lí khi nút cancel được click

  function onBtnCancelClick(){
    console.log("ID: " + gId);
    var vObjectRequest = {
          trangThai: "cancel",
          id: 0,
          orderId: "",
          kichCo: "",
          duongKinh: "",
          loaiPizza: "",
          idVourcher: "",
          giamGia: "",
          idLoaiNuocUong: "",
          soLuongNuoc: "",
          hoTen: "",
          email: "",
          soDienThoai: "",
          loiNhan: "",
          ngayTao: "",
          ngayCapNhat: "",
    }
    //b1: thu thập dữ liệu
    getData(vObjectRequest);
    //b2 check dữ liệu
    //b3 call api
    putApi(vObjectRequest);
    //b4: load lại dữ liệu để kiểm tra cập nhật
    reloadDataToTable(vObjectRequest);
  }
  
  //Region 4: vùng khai báo hàm dùng chung
  function loadDataToTable(paramObj){
    "use strict";
    var vTable = $("#tbl-order").DataTable();
    vTable.clear();
    vTable.rows.add(paramObj);
    vTable.draw();
  }

  function onBtnDetailClick(paramButton){
    "use strict";
    var vRowSelect = $(paramButton).closest("tr");
    var vTable = $("#tbl-order").DataTable();
    var vDataRow = vTable.row(vRowSelect).data();
    gId = vDataRow.id;
    gOrderId = vDataRow.orderId;
    console.log(gId);
    console.log(gOrderId);
    getDetailOrderApi(gOrderId);
    $("#infor-order-modal").modal("show");
  }

  //Hàm load danh sách đồ uống
function loadDrinkList() {
  $.ajax({
    url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
    contentType: "json",
    type: "GET",
    success: function (res) {
      loadDrinkOption(res);
    },
  });
}
//Hàm gán dữ liệu vào ô select
function loadDrinkOption(paramDrinkObj) {
  Array.from(paramDrinkObj).forEach((drink) => {
    $("#select-drink").append(
      `<option value='${drink.maNuocUong}'>${drink.tenNuocUong}</option>`
    );
  });
}
  
  //hàm tạo option cho select pizza
  function getPizzaSize(paramPizza){
    "use strict";
    $.each(paramPizza,function(i,item){
      $("#select-pizzasize").append($('<option>',{
        text: item,
        value: item
      }))
    })
  }

  //hàm gọi api order theo order id
  function getDetailOrderApi(paramOrderId){
    "use strict";
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + paramOrderId,
      type:"GET",
      dataType:"json",
      success: function(res){
        hanldeOrderDetail(res);
        console.log(res);
      },
      error: function(error){
        alert(error.responseText);
      }
    })
  }

  //hàm đổ data vào modal
  function hanldeOrderDetail(paramResponse){
    "use strict";
    $("#inp-id").val(paramResponse.id);
    $("#inp-orderid").val(paramResponse.orderId);
    $("#select-pizzasize").val(paramResponse.kichCo);
    $("#inp-duong-kinh").val(paramResponse.duongKinh);

    $("#inp-salad").val(paramResponse.salad);
    $("#inp-suon-nuong").val(paramResponse.suon);
    $("#inp-type-pizza").val(paramResponse.loaiPizza);
    $("#inp-voucherid").val(paramResponse.idVourcher);

    $("#inp-total-price").val(paramResponse.thanhTien);
    $("#inp-discount").val(paramResponse.giamGia);
    $("#select-drink").val(paramResponse.idLoaiNuocUong);
    $("#inp-number-drink").val(paramResponse.soLuongNuoc);

    $("#inp-fullname").val(paramResponse.hoTen);
    $("#inp-email").val(paramResponse.email);
    $("#inp-phone").val(paramResponse.soDienThoai);
    $("#inp-address").val(paramResponse.diaChi);
    $("#inp-message").val(paramResponse.loiNhan);
    $("#inp-status").val(paramResponse.trangThai);

    // $("#inp-create-date").val(paramResponse.ngayTao);
    // $("#inp-update-date").val(paramResponse.ngayCapNhat);
    $("#inp-create-date").val(moment(paramResponse.ngayTao).format("DD-MM-YYYY"));

    $("#inp-update-date").val(moment(paramResponse.ngayCapNhat).format("DD-MM-YYYY"))
  }

  //hàm thu thập dữ liệu order trên modal
  function getData(paramObj){
    "use strict";
    paramObj.id=  $("#inp-id").val()
    paramObj.orderId=  $("#inp-orderid").val()
    paramObj.kichCo=  $("#select-pizzasize").val()
    paramObj.duongKinh=  $("#inp-duong-kinh").val();

    paramObj.salad = $("#inp-salad").val();
    paramObj.suon = $("#inp-suon-nuong").val();
    paramObj.loaiPizza = $("#inp-type-pizza").val();
    paramObj.idVourcher = $("#inp-voucherid").val();

    paramObj.thanhTien = $("#inp-total-price").val();
    paramObj.giamGia = $("#inp-discount").val();
    paramObj.idLoaiNuocUong = $("#select-drink").val();
    paramObj.soLuongNuoc = $("#inp-number-drink").val();
    paramObj.hoTen = $("#inp-fullname").val();
    paramObj.email = $("#inp-email").val();
    paramObj.soDienThoai = $("#inp-phone").val();
    paramObj.diaChi = $("#inp-address").val();
    paramObj.loiNhan = $("#inp-message").val();

    // paramObj.ngayTao = $("#inp-create-date").val().moment().;
    // paramObj.ngayCapNhat = $("#inp-update-date").val();
  }

  //hàm update thông tin order
  function putApi(paramObj){
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId,
      type: "PUT",
      async: false,
      data:JSON.stringify(paramObj),
      contentType: "application/json;charset=UTF-8",
      success:function(res){
        console.log(res);
        alert("cập nhật trạng thái thành công user có orderId=" + gOrderId);
        // location.reload();
        $("#infor-order-modal").modal("hide");
      },
      error: function(ajaxContext){
        alert(ajaxContext.responseText);
      }
    })

    
  }

  
  function loadDataToTable1(params){
    "use strict";
    var vTable = $("#tbl-order").DataTable();
    vTable.clear();
    vTable.row.add(params);
    vTable.draw();
  }
  // hàm reload trang
  function reloadDataToTable(paramId){
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + paramId.orderId,
      type: "GET",
      dataType: "json",
      success:function(res){
        gDataOrder1 = res;
        console.log(gDataOrder1);
        loadDataToTable1(gDataOrder1);
        setTimeout(function(){
          location.reload()
        },3000)
        
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });
  }
  // // hàm reload trang
  // function reloadDataToTable(paramId){
  //   $.ajax({
  //     url: "http://42.115.221.44:8080/devcamp-pizza365/orders" ,
  //     type: "GET",
  //     dataType: "json",
  //     success:function(res){
  //       gDataOrder = res;
  //       console.log(gDataOrder);
  //       loadDataToTable(gDataOrder);
        
  //     },
  //     error: function(error){
  //       console.assert(error.responseText);
  //     }
  //   });
  // }
});